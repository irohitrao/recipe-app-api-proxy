# recipe-app-api-proxy

# Recipe app API proxy application

NGINX proxy app for our recipe app API

## USAGE

### Environment Variables

* 'LISTEN_PORT' - Port to listen to (Default: '8000')

